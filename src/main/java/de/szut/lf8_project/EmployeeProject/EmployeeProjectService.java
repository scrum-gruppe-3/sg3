package de.szut.lf8_project.EmployeeProject;

import de.szut.lf8_project.exceptionHandling.ResourceNotFoundException;
import de.szut.lf8_project.projects.ProjectsEntity;
import de.szut.lf8_project.projects.ProjectsRepository;
import de.szut.lf8_project.EmployeeProject.Dto.EmployeeNameAndSkillData;
import de.szut.lf8_project.EmployeeProject.Dto.EmployeeProjectRoleDto;
import de.szut.lf8_project.EmployeeProject.Dto.QualificationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class EmployeeProjectService {
    private RestTemplate restTemplate = new RestTemplate();
    @Autowired
    private EmployeeProjectRepository employeeProjectRepo;
    @Autowired
    private ProjectsRepository projectsRepo;

    EmployeeProjectService() {

    }

    public List<EmployeeProjectEntity> getAllEmployeesByProject(long id) {
        return this.employeeProjectRepo.findByProjectId(id);
    }

    public Optional<ProjectsEntity> addEmployees(EmployeeProjectRoleDto employeeProjectRoleDto) {
        var projectEntity = this.projectsRepo.findById(employeeProjectRoleDto.getProjectId());
        var newEmployeeEntity = new EmployeeProjectEntity(projectEntity, employeeProjectRoleDto.getEmployeeId(), employeeProjectRoleDto.getEmployeeRole());
        this.employeeProjectRepo.save(newEmployeeEntity);
        projectEntity.get().getEmployees().add(newEmployeeEntity);
        return projectEntity;
    }

    public Optional<EmployeeProjectEntity> findEmployeeProjectById (long projectId, long employeeId) {
        return this.employeeProjectRepo.findByProjectIdAndEmployeeId(projectId, employeeId);
    }

    @Transactional
    public void deleteEmployeeFromProject (EmployeeProjectEntity employee, ProjectsEntity project) {
        employee.setProject(null);
        project.getEmployees().remove(employee);
        this.employeeProjectRepo.delete(employee);
    }

    public List<EmployeeProjectEntity> getAllProjectsOfEmployee(long id){
        return this.employeeProjectRepo.findByEmployeeId(id);
    }

    public ResponseEntity<EmployeeNameAndSkillData> getEmployeeQualification(long id, String token) {
        HttpHeaders httpHeaders = new HttpHeaders();
        String tokenSplit = token.split(" ") [1];
        httpHeaders.setBearerAuth(tokenSplit);
        HttpEntity<String> entity = new HttpEntity<>("body", httpHeaders);
        String queryUrl = String.format("https://employee.szut.dev/employees/%s/qualifications", id);
        return this.restTemplate.exchange(queryUrl, HttpMethod.GET, entity, EmployeeNameAndSkillData.class);
    }

    public void checkIfEmployeeExists(long id, String token) {
        HttpHeaders httpHeaders = new HttpHeaders();
        String tokenSplit = token.split(" ") [1];
        httpHeaders.setBearerAuth(tokenSplit);
        HttpEntity<String> entity = new HttpEntity<>("body", httpHeaders);
        String queryUrl = String.format("https://employee.szut.dev/employees/%s/", id);
        try{ this.restTemplate.exchange(queryUrl, HttpMethod.GET, entity, EmployeeResponseDto.class);
        }
        catch (HttpClientErrorException e) {
            switch (e.getRawStatusCode()) {
                case 404:
                    throw new ResourceNotFoundException(String.format("Employee %s doesn't exist", id));
                case 401:
                    throw new UnauthorizedException("Unauthorized");
                default:
                    throw new InternalServerErrorException("Something went wrong");
            }
        }
    }

    public void checkEmployeeQualification(EmployeeProjectRoleDto employeeProjectRoleDto, String token) {
        var responseEntity = getEmployeeQualification(employeeProjectRoleDto.getEmployeeId(), token);
        if(responseEntity.getStatusCodeValue()  == 404) {
            throw new ResourceNotFoundException("Employee not found on id = " + employeeProjectRoleDto.getEmployeeId());
        }
        else{
            for (QualificationDto skill : responseEntity.getBody().getSkillSet()) {
                if (skill.designation.equals(employeeProjectRoleDto.getEmployeeRole())) {
                    return true;
                }
            }
            throw new ResourceNotFoundException(String.format("Employee %s doesn't have the necessary skill for destined role", employeeProjectRoleDto.getEmployeeId()));
        }
    }

    public void checkEmployeeProjectsDuringTimePeriod(EmployeeProjectRoleDto employeeProjectRoleDto, ProjectsEntity projectsEntity) {
        if(!this.employeeProjectRepo.findByEmployeeIdAndStartDateAndEndDate(employeeProjectRoleDto.getEmployeeId(), projectsEntity.getStartDate(), projectsEntity.getEndDate()).isEmpty()) {
            throw new ResourceNotFoundException(String.format("Employee %s is already planned for another project during time period", employeeProjectRoleDto.getEmployeeId()));
        }
    }

    public void checkIfEmployeeAlreadyInProject(EmployeeProjectRoleDto employeeProjectRoleDto) {
        var project = this.projectsRepo.findById(employeeProjectRoleDto.getProjectId());
        var employeeMap = project.get().getEmployees().stream().collect(Collectors.toMap(EmployeeProjectEntity::getEmployeeId, Function.identity()));
        if(employeeMap.get(employeeProjectRoleDto.getEmployeeId()) != null) {
            throw new ResourceNotFoundException(String.format("Employee %s is already part of project %s", employeeProjectRoleDto.getEmployeeId(), employeeProjectRoleDto.getProjectId()));
        }
    }
}


