package de.szut.lf8_project.EmployeeProject;

import com.fasterxml.jackson.annotation.JsonIgnore;

import de.szut.lf8_project.projects.ProjectsEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Optional;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "EmployeeProject")
public class EmployeeProjectEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private long LineId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="contact", nullable = false)
    @JsonIgnore
    private ProjectsEntity project;
    private long employeeId;
    private String employeeRole;

    public EmployeeProjectEntity(long employeeId, String employeeRole) {
        this.employeeId = employeeId;
        this.employeeRole = employeeRole;
    }

    public EmployeeProjectEntity(Optional<ProjectsEntity> projectEntity, long employeeId, String employeeRole) {
        this.project = projectEntity.get();
        this.employeeId = employeeId;
        this.employeeRole = employeeRole;
    }

}