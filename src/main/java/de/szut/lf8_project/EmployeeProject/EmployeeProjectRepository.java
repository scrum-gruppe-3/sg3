package de.szut.lf8_project.EmployeeProject;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface EmployeeProjectRepository extends JpaRepository<EmployeeProjectEntity, Long> {

    List<EmployeeProjectEntity> findByProjectId(long id);
    Optional<EmployeeProjectEntity> findByProjectIdAndEmployeeId(long projectId, long employeeId);
    List<EmployeeProjectEntity> findByEmployeeId(long id);
    @Query("SELECT e FROM EmployeeProjectEntity e WHERE e.employeeId = :employeeId and ((e.project.endDate >= :startDate and e.project.startDate <= :endDate) or (e.project.startDate >= :startDate and e.project.endDate <= :endDate))")
    List<EmployeeProjectEntity> findByEmployeeIdAndStartDateAndEndDate (long employeeId, Date startDate, Date endDate);

}