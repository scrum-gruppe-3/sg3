package de.szut.lf8_project.EmployeeProject;

import de.szut.lf8_project.EmployeeProject.Dto.AddEmployeeResponseDto;
import de.szut.lf8_project.EmployeeProject.Dto.EmployeeProjectRoleDto;
import de.szut.lf8_project.EmployeeProject.Dto.ProjectsOfEmployeeDto;
import de.szut.lf8_project.projects.ProjectsEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EmployeeProjectMapper {

    public EmployeeProjectRoleDto mapEntityToEmployeeGetDto(EmployeeProjectEntity entity) {
        return new EmployeeProjectRoleDto(entity.getProject().getId(), entity.getEmployeeId(), entity.getEmployeeRole());
    }

    public AddEmployeeResponseDto mapProjectEntityToAddEmployeeResponseDto(Optional<ProjectsEntity> projectsEntity) {
        return new AddEmployeeResponseDto(projectsEntity.get().getId(), projectsEntity.get().getProjectDescription(), projectsEntity.get().getEmployees());
    }

    public ProjectsOfEmployeeDto mapEmployeeProjectToProjectsOfEmployeeDto(EmployeeProjectEntity employeeProject){
        return new ProjectsOfEmployeeDto(employeeProject.getProject().getId(), employeeProject.getProject().getProjectDescription(), employeeProject.getProject().getStartDate(), employeeProject.getProject().getEndDate(), employeeProject.getEmployeeRole());
    }

    public EmployeeProjectRoleDto mapEmployeeProjectEntityToEmployeeProjectRoleDto(EmployeeProjectEntity employeeProjectEntity) {
        return new EmployeeProjectRoleDto(employeeProjectEntity.getProject().getId(), employeeProjectEntity.getEmployeeId(), employeeProjectEntity.getEmployeeRole());
    }
}
