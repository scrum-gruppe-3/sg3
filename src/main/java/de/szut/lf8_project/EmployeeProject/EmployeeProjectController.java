package de.szut.lf8_project.EmployeeProject;

import de.szut.lf8_project.EmployeeProject.Dto.AddEmployeeResponseDto;
import de.szut.lf8_project.EmployeeProject.Dto.EmployeeProjectRoleDto;
import de.szut.lf8_project.EmployeeProject.Dto.GetProjectsOfEmployeeDto;
import de.szut.lf8_project.exceptionHandling.ResourceNotFoundException;
import de.szut.lf8_project.projects.ProjectsService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "employeeproject")

public class EmployeeProjectController {
    @Autowired
    private EmployeeProjectService employeeProjectService;
    @Autowired
    private ProjectsService projectsService;
    @Autowired
    private EmployeeProjectMapper employeeProjectMapper;

    @Operation(summary = "get all employees for a project by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "find successful"),

            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "resource not found",
                    content = @Content)})
    @GetMapping("/{id}")
    public ResponseEntity<List<EmployeeProjectRoleDto>> getProjectEmployeesById(@PathVariable int id) {;
        var a = this.employeeProjectService.getAllEmployeesByProject(id)
                .stream()
                .map(e -> this.employeeProjectMapper.mapEntityToEmployeeGetDto(e))
                .collect(Collectors.toList());
        if(!a.isEmpty()){
            return new ResponseEntity<List<EmployeeProjectRoleDto>>(a, HttpStatus.OK);
        } else {
            throw new ResourceNotFoundException("Project id not Found!");
        }
    }

    @Operation(summary = "Add a new Employee to an existing project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "add employee accepted",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = AddEmployeeResponseDto.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @PostMapping()
    public ResponseEntity<AddEmployeeResponseDto> addEmployeeToProject(@RequestHeader("Authorization")  String token, @RequestBody @Valid EmployeeProjectRoleDto employeeProjectRoleDto){
        var projectEntity = this.projectsService.readById(employeeProjectRoleDto.getProjectId());
        this.employeeProjectService.checkIfEmployeeAlreadyInProject(employeeProjectRoleDto);
        this.employeeProjectService.checkEmployeeQualification(employeeProjectRoleDto, token);
        this.employeeProjectService.checkEmployeeProjectsDuringTimePeriod(employeeProjectRoleDto, projectEntity);
        var res = this.employeeProjectMapper.mapProjectEntityToAddEmployeeResponseDto(this.employeeProjectService.addEmployees(employeeProjectRoleDto));
        return new ResponseEntity<AddEmployeeResponseDto>(res, HttpStatus.ACCEPTED);
    }

    @Operation(summary = "Delete an Employee from an existing project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "deleted employee",
                    content = {@Content(mediaType = "application/json"
                    )}),
            @ApiResponse(responseCode = "404", description = "resource not found",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @DeleteMapping
    public ResponseEntity<Void> deleteEmployeeFromProject(@RequestParam long projectId, @RequestParam long employeeId){
        var project = this.projectsService.readById(projectId);
        var entity = this.employeeProjectService.findEmployeeProjectById(projectId, employeeId);
        if (entity.isEmpty()) {
            throw new ResourceNotFoundException("Employee not found in project:" + projectId);
        } else {
            this.employeeProjectService.deleteEmployeeFromProject(entity.get(), project);
            return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
        }
    }

    @Operation(summary = "get all projects for an employee by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "find successful",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GetProjectsOfEmployeeDto.class))}),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "resource not found",
                    content = @Content)})
    @GetMapping("/employee/{id}")
    public ResponseEntity<GetProjectsOfEmployeeDto> getProjectsOfEmployee(@PathVariable int id) {
        var a = new GetProjectsOfEmployeeDto(id, this.employeeProjectService.getAllProjectsOfEmployee(id)
                .stream()
                .map(e -> this.employeeProjectMapper.mapEmployeeProjectToProjectsOfEmployeeDto(e))
                .collect(Collectors.toList())
        );
        if(a != null){
            return new ResponseEntity<GetProjectsOfEmployeeDto>(a, HttpStatus.OK);
        }
        else {
            throw new ResourceNotFoundException("No projects found!");
        }
    }
}
