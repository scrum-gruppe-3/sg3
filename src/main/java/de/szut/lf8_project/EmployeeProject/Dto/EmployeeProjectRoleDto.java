package de.szut.lf8_project.EmployeeProject.Dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class EmployeeProjectRoleDto {

    private long projectId;
    private long employeeId;
    private String employeeRole;

}