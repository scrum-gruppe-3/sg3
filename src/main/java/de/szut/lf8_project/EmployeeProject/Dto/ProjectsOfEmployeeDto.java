package de.szut.lf8_project.EmployeeProject.Dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@Getter
@Setter
public class ProjectsOfEmployeeDto {
        private long projectId;
        private String projectDescription;
        @JsonFormat(shape = JsonFormat.Shape.STRING)
        private Date startDate;
        @JsonFormat(shape = JsonFormat.Shape.STRING)
        private Date endDate;
        private String projectRole;
    }
