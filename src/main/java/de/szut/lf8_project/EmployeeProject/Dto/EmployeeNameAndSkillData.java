package de.szut.lf8_project.EmployeeProject.Dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class EmployeeNameAndSkillData {

    public int id;
    public String firstname;
    public String lastname;
    public List<QualificationDto> skillSet;

}
