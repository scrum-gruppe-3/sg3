package de.szut.lf8_project.EmployeeProject.Dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeCreateDto {

    private long employeeId;
    private String employeeRole;

    @JsonCreator
    EmployeCreateDto(@JsonProperty long employeeId, @JsonProperty String employeeRole) {
        this.employeeId = employeeId;
        this.employeeRole = employeeRole;
    }

}
