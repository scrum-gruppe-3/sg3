package de.szut.lf8_project.EmployeeProject.Dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GetProjectsOfEmployeeDto {

    private long employeeId;
    private List<ProjectsOfEmployeeDto> projects;
}
