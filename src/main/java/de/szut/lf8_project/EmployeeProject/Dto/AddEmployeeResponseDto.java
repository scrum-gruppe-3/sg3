package de.szut.lf8_project.EmployeeProject.Dto;

import de.szut.lf8_project.EmployeeProject.EmployeeProjectEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AddEmployeeResponseDto {

    private long projectId;
    private String projectDescription;
    private List<EmployeeProjectEntity> employeeList;

}
