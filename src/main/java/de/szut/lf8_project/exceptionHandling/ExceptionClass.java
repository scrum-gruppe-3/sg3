package de.szut.lf8_project.exceptionHandling;

public class ExceptionClass extends RuntimeException {

    public ExceptionClass(String message) {
        super(message);
    }

}