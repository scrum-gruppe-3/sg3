package de.szut.lf8_project.Services;

import de.szut.lf8_project.projects.*;
import org.springframework.http.ResponseEntity;

public class ValidationService {

    private ProjectsRepository PRepo;
    private ProjectsService PServ;
    private ProjectsController Pcon;
    public enum Mode {
        CREATE,
        UPDATE
    }

    ValidationService(ProjectsService xPServ, ProjectsRepository xPRepo, ProjectsController xPCon) {
        this.Pcon =xPCon;
        this.PRepo =xPRepo;
        this.PServ =xPServ;
    }

   boolean checkValidProjectID(Mode xMode, ProjectsEntity xProject) {
        ResponseEntity<ProjectsEntity> TestEntity;
        TestEntity = Pcon.getProjectById((int)xProject.getId());

        if (TestEntity != null) {
            return false;
        } else {
            return true;
        }
    }

}
