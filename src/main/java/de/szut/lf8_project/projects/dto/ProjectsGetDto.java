package de.szut.lf8_project.projects.dto;

import de.szut.lf8_project.EmployeeProject.EmployeeProjectEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@AllArgsConstructor
@Getter
@Setter
public class ProjectsGetDto {

    private long id;
    private long workerId;
    private long customerId;
    private String contact;
    private String projectGoal;
    private String projectDescription;
    private Date startDate;
    private Date endDate;
    private List<EmployeeProjectEntity> employees;

    ProjectsGetDto(long id, String projectDescription, Date startDate, Date endDate, String employeeRole) {
        this.id = id;
        this.projectDescription = projectDescription;
        this.startDate = startDate;
        this.endDate = endDate;
    }

}
