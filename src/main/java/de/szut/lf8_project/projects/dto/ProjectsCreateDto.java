package de.szut.lf8_project.projects.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import de.szut.lf8_project.EmployeeProject.Dto.EmployeCreateDto;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.ElementCollection;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class ProjectsCreateDto {

    private long workerId;
    private long customerId;
    private String contact;
    private String projectGoal;
    private String projectDescription;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Date startDate;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Date endDate;
    @ElementCollection
    private List<EmployeCreateDto> employees;

    @JsonCreator
    public ProjectsCreateDto(@JsonProperty("workerId") long workerId, @JsonProperty("customerId") long customerId, @JsonProperty("contact") String contact, @JsonProperty("projectGoal") String projectGoal, @JsonProperty("projectDescription")String projectDescription, @JsonProperty("startDate")Date startDate, @JsonProperty("endDate")Date endDate, @JsonProperty("employees") List<EmployeCreateDto> employees) {
        this.workerId = workerId;
        this.customerId = customerId;
        this.contact = contact;
        this.projectGoal = projectGoal;
        this.projectDescription = projectDescription;
        this.startDate = startDate;
        this.endDate = endDate;
        this.employees = employees;
    }

}
