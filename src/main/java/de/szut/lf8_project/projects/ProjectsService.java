package de.szut.lf8_project.projects;

import de.szut.lf8_project.EmployeeProject.EmployeeProjectEntity;
import de.szut.lf8_project.EmployeeProject.EmployeeProjectMapper;
import de.szut.lf8_project.EmployeeProject.EmployeeProjectRepository;
import de.szut.lf8_project.EmployeeProject.Dto.EmployeeNameAndSkillData;
import de.szut.lf8_project.EmployeeProject.Dto.EmployeeProjectRoleDto;
import de.szut.lf8_project.EmployeeProject.Dto.QualificationDto;
import de.szut.lf8_project.EmployeeProject.EmployeeProjectService;

import de.szut.lf8_project.exceptionHandling.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@EnableTransactionManagement
public class ProjectsService {

    @Autowired
    private ProjectsRepository repo;
    @Autowired
    private EmployeeProjectService employeeProjectService;
    @Autowired
    private EmployeeProjectMapper employeeProjectMapper;
    private RestTemplate restTemplate = new RestTemplate();

    public ProjectsService() {

    }

    public ProjectsEntity create(ProjectsEntity entity) {
        return this.repo.save(entity);
    }

    public List<ProjectsEntity> readAll() {
        return this.repo.findAll();
    }

    public ProjectsEntity readById(long id) {
        Optional<ProjectsEntity> optQualification = this.repo.findById(id);

        if (optQualification.isEmpty()) {
            return null;
        }

        return optQualification.get();
    }

    public void delete(ProjectsEntity entity) {
        this.repo.delete(entity);
    }

    public void update(ProjectsEntity newEntity, int id, String token){
        var oldProject = this.repo.findById(id).get();
        oldProject.setProjectGoal(newEntity.getProjectGoal());
        oldProject.setProjectDescription(newEntity.getProjectDescription());
        oldProject.setEmployees(updateEmployeeInformation(oldProject.getEmployees(), newEntity.getEmployees()));
        oldProject.setContact(newEntity.getContact());
        oldProject.setCustomerId(newEntity.getCustomerId());
        oldProject.setEndDate(newEntity.getEndDate());
        oldProject.setStartDate(newEntity.getStartDate());
        oldProject.setWorkerId(newEntity.getWorkerId());
        oldProject.getEmployees().forEach( e-> {
            this.employeeProjectService.checkEmployeeProjectsDuringTimePeriod(this.employeeProjectMapper.mapEmployeeProjectEntityToEmployeeProjectRoleDto(e), oldProject);
            this.employeeProjectService.checkEmployeeQualification(this.employeeProjectMapper.mapEmployeeProjectEntityToEmployeeProjectRoleDto(e), token);
        });
        this.repo.save(oldProject);
    }

    private List<EmployeeProjectEntity> updateEmployeeInformation(List<EmployeeProjectEntity> oldEmployeeList, List<EmployeeProjectEntity> newEmployeeList){
        var employeeMap = oldEmployeeList.stream().collect(Collectors.toMap(EmployeeProjectEntity::getEmployeeId, Function.identity()));
        if(employeeMap.size() > newEmployeeList.size()){
            throw new ResourceNotFoundException("Please use the correct Endpoint to remove employees");
        }
        newEmployeeList.forEach(e -> {
            if(employeeMap.containsKey(e.getEmployeeId())) {
                employeeMap.get(e.getEmployeeId()).setEmployeeRole(e.getEmployeeRole());
            }
            else {
                throw new ResourceNotFoundException(String.format("Employee %s is not part of the chosen project. Use correct endpoint to add employees.", e.getEmployeeId()));
            }
        });
        return employeeMap.values().stream().collect(Collectors.toList());
    }
}

