package de.szut.lf8_project.projects;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import de.szut.lf8_project.EmployeeProject.EmployeeProjectEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

import java.util.Date;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "projects")
public class ProjectsEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private long id;
    private long workerId;
    private long customerId;
    private String contact;
    private String projectGoal;
    private String projectDescription;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Date startDate;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Date endDate;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<de.szut.lf8_project.EmployeeProject.EmployeeProjectEntity> employees;

    public ProjectsEntity(long workerId, long customerId, String contact, String projectGoal, String projectDescription, Date startDate, Date endDate, List<EmployeeProjectEntity> EmployeeProjectEntity) {
        this.workerId = workerId;
        this.customerId = customerId;
        this.contact = contact;
        this.projectGoal = projectGoal;
        this.projectDescription = projectDescription;
        this.startDate = startDate;
        this.endDate = endDate;
        this.employees = EmployeeProjectEntity;
    }

}
