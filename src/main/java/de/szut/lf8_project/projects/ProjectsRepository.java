package de.szut.lf8_project.projects;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface ProjectsRepository extends JpaRepository<ProjectsEntity, Long> {

    Optional<ProjectsEntity> findById(long id);
}