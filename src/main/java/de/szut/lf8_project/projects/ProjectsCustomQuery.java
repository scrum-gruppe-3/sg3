package de.szut.lf8_project.projects;

import java.util.Date;

public interface ProjectsCustomQuery {

    long getId();
    String getProjectDescription();
    Date getStartDate();
    Date getEndDate();

}