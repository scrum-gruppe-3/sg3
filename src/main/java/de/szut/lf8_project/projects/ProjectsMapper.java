package de.szut.lf8_project.projects;

import de.szut.lf8_project.EmployeeProject.Dto.AddEmployeeResponseDto;
import de.szut.lf8_project.EmployeeProject.Dto.EmployeCreateDto;
import de.szut.lf8_project.EmployeeProject.Dto.EmployeeProjectRoleDto;
import de.szut.lf8_project.EmployeeProject.Dto.ProjectsOfEmployeeDto;
import de.szut.lf8_project.EmployeeProject.EmployeeProjectEntity;
import de.szut.lf8_project.projects.dto.*;

import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProjectsMapper {

    public ProjectsGetDto mapToGetDto(ProjectsEntity entity) {
        return new ProjectsGetDto(entity.getId(), entity.getWorkerId(), entity.getCustomerId(), entity.getContact(),entity.getProjectGoal(),entity.getProjectDescription(),entity.getStartDate(),entity.getEndDate(), entity.getEmployees());
    }

    public ProjectsEntity mapCreateDtoToEntity(ProjectsCreateDto dto) {
        var entity = new ProjectsEntity();
        entity.setWorkerId(dto.getWorkerId());
        entity.setCustomerId(dto.getCustomerId());
        entity.setContact(dto.getContact());
        entity.setProjectGoal(dto.getProjectGoal());
        entity.setProjectDescription(dto.getProjectDescription());
        entity.setStartDate(dto.getStartDate());
        entity.setEndDate(dto.getEndDate());
        entity.setEmployees(dto.getEmployees().stream().map(e -> mapCreateEmployeeDtoToEntity(e)).collect(Collectors.toList()));

        return entity;
    }

    public EmployeeProjectEntity mapCreateEmployeeDtoToEntity(EmployeCreateDto dto) {
        var entity = new EmployeeProjectEntity();
        entity.setEmployeeId(dto.getEmployeeId());
        entity.setEmployeeRole(dto.getEmployeeRole());

        return entity;
    }
}
