package de.szut.lf8_project.projects;

import de.szut.lf8_project.EmployeeProject.Dto.AddEmployeeResponseDto;
import de.szut.lf8_project.EmployeeProject.Dto.EmployeeProjectRoleDto;
import de.szut.lf8_project.EmployeeProject.Dto.GetProjectsOfEmployeeDto;
import de.szut.lf8_project.EmployeeProject.EmployeeProjectEntity;
import de.szut.lf8_project.EmployeeProject.EmployeeProjectMapper;
import de.szut.lf8_project.EmployeeProject.EmployeeProjectService;
import de.szut.lf8_project.exceptionHandling.ResourceNotFoundException;
import de.szut.lf8_project.projects.dto.*;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "project")

public class ProjectsController {
    @Autowired
    private ProjectsService service;
    @Autowired
    private EmployeeProjectService employeeProjectService;
    @Autowired
    private ProjectsMapper projectsMapper;
    @Autowired
    private EmployeeProjectMapper employeeProjectMapper;

    public ProjectsController() {
    }

    @Operation(summary = "Create a new project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "created project",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectsGetDto.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @PostMapping
    public ResponseEntity<ProjectsGetDto> create(@RequestHeader("Authorization") String token, @RequestBody @Valid ProjectsCreateDto projectsCreateDto){
        ProjectsEntity projectsEntity = this.projectsMapper.mapCreateDtoToEntity(projectsCreateDto);

        for (EmployeeProjectEntity employee : projectsEntity.getEmployees()) {
            employee.setProject(projectsEntity);
            this.employeeProjectService.checkEmployeeQualification(this.employeeProjectMapper.mapEmployeeProjectEntityToEmployeeProjectRoleDto(employee), token);
            this.employeeProjectService.checkEmployeeProjectsDuringTimePeriod(this.employeeProjectMapper.mapEmployeeProjectEntityToEmployeeProjectRoleDto(employee), projectsEntity);
        }
        projectsEntity = this.service.create(projectsEntity);
        var res =  this.projectsMapper.mapToGetDto(projectsEntity);
        return new ResponseEntity<ProjectsGetDto>(res, HttpStatus.CREATED);
    }

    @Operation(summary = "Provides a list of all projects")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "list of projects",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectsGetDto.class))}),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @GetMapping
    public ResponseEntity<List<ProjectsGetDto>> findAll() {
        var res =  this.service
            .readAll()
            .stream()
            .map(e -> this.projectsMapper.mapToGetDto(e))
            .collect(Collectors.toList());
        return new ResponseEntity<List<ProjectsGetDto>>(res, HttpStatus.OK);
    }

    @Operation(summary = "Deletes a project by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "delete successful"),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "resource not found",
                    content = @Content)})
    @DeleteMapping
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> deleteHelloById(@RequestParam long id) {
        var entity = this.service.readById(id);

        if (entity == null) {
            throw new ResourceNotFoundException("ProjectEntity not found on id = " + id);
        } else {
            this.service.delete(entity);
            return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
        }
    }

    @Operation(summary = "Get project by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "find successful"),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "resource not found",
                    content = @Content)})
    @GetMapping("/projects/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<ProjectsEntity> getProjectById( @PathVariable int id) {
        var entity = this.service.readById(id);

        if (entity == null) {
            throw new ResourceNotFoundException("ProjectEntity not found on id = " + id);
        } else {
            return new ResponseEntity<ProjectsEntity>(entity, HttpStatus.OK);
        }
    }

    @Operation(summary = "Update project by id")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "202", description = "update accepted"),
        @ApiResponse(responseCode = "401", description = "not authorized",
            content = @Content),
        @ApiResponse(responseCode = "404", description = "resource not found",
            content = @Content)})
    @PutMapping("/projects/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<Void> updateProjectById( @RequestBody ProjectsEntity xProject, @PathVariable int id, @RequestHeader("Authorization") String token) {
        var entity = this.service.readById(id);

        if (entity == null) {
            throw new ResourceNotFoundException("ProjectEntity not found on id = " + id);
        } else {
            this.service.update(xProject, id, token);

            return new ResponseEntity<Void>(HttpStatus.ACCEPTED);
        }
    }

}
